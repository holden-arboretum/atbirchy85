# ATBirchy85

A library written for a master Arduino device in order to communicate with the holden arboretum optical sensor.

## Examples

Initialize the library with pin 10 being the chip select:
```
ATBirchy85 birchy(10);
```
Quickly test to see if SPI is working correctly:
```
Serial.println(birchy.spi_test() ? "SPI is working properly" : "SPI is not working...");
```
See if we can store a calibration constant and then retrieve it:
```
// store our eeprom value
uint8_t constant = 5;
birchy.store_calibration(5);

// read our calibration constant
uint8_t recieved = birchy.read_calibration();
Serial.print("Got: ");
Serial.print(recieved);
Serial.println("    Expected: 5");
```
Get a reading from the optical sensor in volts:
```
// Get our light value and print it
float value = birchy.get_light();
Serial.print("Got Light Reading of ");
Serial.print(value);
Serial.println("V");
```

## Some notes
- We use a SPI_CLOCK_DIV of 16 so that the bus runs slow enough for our 1Mhz ATtiny85 to process commands

