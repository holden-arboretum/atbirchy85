#include <ATBirchy85.h>

ATBirchy85 birchy(10);

void setup() {
  // initialize Serial
  while (!Serial);
  Serial.begin(9600);
  birchy.reset(10);
  Serial.println(birchy.spi_test() ? "SPI is working properly" : "SPI is not working...");
}

void loop() {
  // store our eeprom value
  uint8_t constant = 5;
  birchy.store_calibration(5);

  // read our calibration constant
  uint8_t recieved = birchy.read_calibration();
  Serial.print("Got: ");
  Serial.print(recieved);
  Serial.println("    Expected: 5");

  // Get our light value and print it
  float value = birchy.get_light();
  Serial.print("Got Light Reading of ");
  Serial.print(value);
  Serial.println("V");
  delay(5000);
}
