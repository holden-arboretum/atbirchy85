/*****************************************************************************
**  File name: ATBirchy85.h                                                  *
**  Description: Driver for optical sensor board                             *
**  Notes:                                                                   *
**  Author(s): Humphrey, Dylan                                               *
**  Created: 3/26/2019                                                       *
**  Last Modified: 3/26/2019                                                 *
**  Changes:                                                                 *
******************************************************************************/

#ifndef ATBIRCHY85_H
#define ATBIRCHY85_H

#include "Arduino.h"
#include "SPI.h"

union Float {
  float val;
  uint8_t bytes[4];
};

class ATBirchy85 {
private:
  uint8_t chip_select;
  uint8_t spi_transfer(uint8_t data, bool del);
public:

  /** Create a ATBirchy85 instance
   *
   *  @param ss The chip select pin for SPI
   */
  ATBirchy85(uint8_t ss);

  /** Read the current light value from the sensor
   *
   *  @returns the float value if successful
   */
  float get_light();

  /** Read the current light value from the sensor
   *
   *  @returns the raw adc value
   */
  float get_light_raw();

  /** Store a calibration constant in EEPROM on the sensor
   *
   *  @param value The calibration constant to be stored
   */
  void store_calibration(uint8_t value);

  /** Read a calibration constant from EEPROM on the sensor
   *
   *  @returns The calibration constant
   */
  uint8_t read_calibration();

  /** Test to make sure the SPI is working correctly
   *
   *  @param do_ss Boolean to specify if chip select should be used or not
   *
   *  @note The reason for the do_ss parameter is to ensure the chip select
   *        functionality of the optical sensor is working properly
   *
   *  @returns true if it is working, false if not
   */
  bool spi_test();

  /** Resets the sensor via the given pin
   *
   *  @param the digital pin that is connected to the reset on the ATBirchy85
   *
   *  @note The pin must be that of a digital pin
   *
   *  @returns The calibration constant
   */
   void reset(int pin);
};

#endif ATBIRCHY85_H
