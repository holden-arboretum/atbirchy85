#include "ATBirchy85.h"

#define BLANK 0x11
#define SPI_TEST 0xFF
#define GET_LIGHT 0x01
#define EEPROM_WRITE 0x02
#define EEPROM_READ 0x03

ATBirchy85::ATBirchy85(uint8_t ss) {
  pinMode(ss, OUTPUT);
  digitalWrite(ss, HIGH);
  chip_select = ss;

  SPI.begin();
  SPI.setBitOrder(MSBFIRST);
  SPI.setClockDivider(SPI_CLOCK_DIV16);
  SPI.setDataMode(SPI_MODE0);
}

float ATBirchy85::get_light() {
  return get_light_raw() * (3.3 / 1024.0);
}

float ATBirchy85::get_light_raw() {
  digitalWrite(chip_select, LOW);
  delay(20);
  union Float value;
  spi_transfer(GET_LIGHT, true);
  int i;
  for (i = 0; i < 4; i++) {
    uint8_t byte = spi_transfer(GET_LIGHT, true);
    value.bytes[i] = byte;
  }
  digitalWrite(chip_select, HIGH);
  return value.val;
}

void ATBirchy85::store_calibration(uint8_t value) {
  digitalWrite(chip_select, LOW);
  delay(20);
  spi_transfer(EEPROM_WRITE, true);
  spi_transfer(value, false);
  delay(5);
  digitalWrite(chip_select, HIGH);
}

uint8_t ATBirchy85::read_calibration() {
  digitalWrite(chip_select, LOW);
  delay(20);
  spi_transfer(EEPROM_READ, true);
  delay(5);
  uint8_t constant = spi_transfer(BLANK, false);
  digitalWrite(chip_select, HIGH);
  return constant;
}

bool ATBirchy85::spi_test() {
  // do a test without chip select
  spi_transfer(SPI_TEST, true);
  uint8_t result1 = spi_transfer(BLANK, true);

  // do a test with chip select
  digitalWrite(chip_select, LOW);
  delay(20);
  spi_transfer(SPI_TEST, true);
  uint8_t result2 = spi_transfer(BLANK, true);
  digitalWrite(chip_select, HIGH);
  
  return result2 == 0x10 && !(result1 == 0x10);
}

/**  A helper function which transmits the data over spi
  *
  *  @param data  The byte of data to send over SPI
  *  @param del   A boolean to specify if there should be a 20 us delay after transfer
  *
  * @returns  The recieved byte from the transfer
*/
uint8_t ATBirchy85::spi_transfer(uint8_t data, bool del) {
  uint8_t byte = SPI.transfer(data);
  if (del) {
    delay(50);
  }
  return byte;
}

void ATBirchy85::reset(int pin) {
  pinMode(pin, OUTPUT);
  digitalWrite(pin, LOW);
  delay(100);
  digitalWrite(pin, HIGH);
  delay(100);
}
